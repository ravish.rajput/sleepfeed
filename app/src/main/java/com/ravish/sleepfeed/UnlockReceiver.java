package com.ravish.sleepfeed;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class UnlockReceiver extends BroadcastReceiver {


    long totalSleepTime = 0,startTime,endTime;

    @Override
    public void onReceive(Context appContext, Intent intent) {
        Intent invok = null;

        if (BuildConfig.DEBUG) {
            Log.i("Mydata", "onReceive: " + intent.getAction());
        }
        if (!isCurrentTimeFallsUnderSleepTime())
        {
            return;
        }

        if (intent.getAction().equalsIgnoreCase(Intent.ACTION_SCREEN_OFF))
        {
            invok = new Intent(appContext, SensorService.class);
            appContext.startService(invok);

        } else if (intent.getAction().equalsIgnoreCase(Intent.ACTION_SCREEN_ON)) {

            if(invok!=null)
                appContext.stopService(invok);

            SharedPreferences sp = MyApp.sharedPreferences;
            startTime = sp.getLong("start_time",0);

            endTime = System.currentTimeMillis();

            totalSleepTime = endTime - startTime;
            SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
            df.setTimeZone(TimeZone.getTimeZone("GMT+0"));
            String time = df.format(totalSleepTime);


            SimpleDateFormat dfdate = new SimpleDateFormat("dd/MM");
            String date = dfdate.format(new Date().getTime());

            if (isTimeGreaterThan2Hours(totalSleepTime)) {
            SharedPreferences.Editor prefEditor = MyApp.sharedPreferences.edit();
            prefEditor.putString(date, time);
            prefEditor.apply();
            prefEditor.commit();
            }
        }
    }
    private boolean isTimeGreaterThan2Hours(long time)
    {
        if (time>7200000){
            return true;
        }
        else return false;
    }

    public static final String inputFormat = "HH:mm:ss";
    private Date currentDate;
    private Date dateCompareOne;
    private Date dateCompareTwo;
    private String compareStringOne = "10:00:00";
    private String compareStringTwo = "7:00:00";
    SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat);

    private boolean isCurrentTimeFallsUnderSleepTime(){
        Calendar now = Calendar.getInstance();

        int hour = now.get(Calendar.HOUR);
        int minute = now.get(Calendar.MINUTE);
        int second = now.get(Calendar.SECOND);

        currentDate = parseDate(hour + ":" + minute + ":" + second);
        dateCompareOne = parseDate(compareStringOne);
        dateCompareTwo = parseDate(compareStringTwo);

        if (currentDate.after(dateCompareOne) && currentDate.before(dateCompareTwo))
        {
            return true;
        }

        else return false;
    }

    private Date parseDate(String date) {

        try {
            return inputParser.parse(date);
        } catch (java.text.ParseException e) {
            return new Date(0);
        }
    }

}