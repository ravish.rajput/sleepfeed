package com.ravish.sleepfeed;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

public class SensorService extends Service implements SensorEventListener{

    private static final String TAG_FOREGROUND_SERVICE = "Mydata";
    private float X, Y, Z;
    private SensorManager sensorManager;
    private Sensor accelerometer;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    private void initSensor()
    {
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
            // success! we have an accelerometer
            accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        X = event.values[0];
        Y = event.values[1];
        Z = event.values[2];
        // for device on table , x=1,-1,y=1,-1,z=10
        Log.d(TAG_FOREGROUND_SERVICE, X+" : "+Y+" : "+Z);

        if (isDeviceIdle())
        {
            SharedPreferences.Editor prefEditor = MyApp.sharedPreferences.edit();
            prefEditor.putLong("start_time", System.currentTimeMillis());
            prefEditor.apply();
            prefEditor.commit();

            stopSelf();
        }


    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        sensorManager.unregisterListener(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        initSensor();

        return START_NOT_STICKY;
    }

    private boolean isDeviceIdle()
    {
        if (-1<=X && X<=1 && -1<=Y && Y<=1 && 8<=Z && Z<=10)
            //if (-1<=X && X<=1 && 8<=Y && Y<=10 && -1<=Z && Z<=1) for testing on emulator
            return true;

        else return false;
    }

}
