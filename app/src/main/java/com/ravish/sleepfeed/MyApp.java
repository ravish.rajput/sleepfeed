package com.ravish.sleepfeed;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.evernote.android.job.JobManager;

public class MyApp extends Application {

    public static SharedPreferences sharedPreferences;
    @Override
    public void onCreate() {
        super.onCreate();
        sharedPreferences = getSharedPreferences("SleepPoc", Context.MODE_PRIVATE);
        JobManager.create(this).addJobCreator(new LockScreenJobCreator());
    }
}
